package logger

import (
	"runtime"
	"strings"
	"sync"

	"codeberg.org/gruf/go-byteutil"
)

// funcName formats a function name to a quickly-readable string.
func funcName(fn *runtime.Func) string {
	if fn == nil {
		return ""
	}

	// Get func name
	// for formatting.
	name := fn.Name()

	// Drop all but the package name and function name, no mod path
	if idx := strings.LastIndex(name, "/"); idx >= 0 {
		name = name[idx+1:]
	}

	const params = `[...]`

	// Drop any generic type parameter markers
	if idx := strings.Index(name, params); idx >= 0 {
		name = name[:idx] + name[idx+len(params):]
	}

	return name
}

// bufpool provides memory
// pool of byte buffers.
var bufpool sync.Pool

// getBuf fetches a byte
// bufferfrom memory pool.
func getBuf() *byteutil.Buffer {
	v := bufpool.Get()
	if v == nil {
		var buf byteutil.Buffer
		buf.B = make([]byte, 0, 512)
		v = (&buf)
	}
	return v.(*byteutil.Buffer)
}

// putBuf resets and places
// buffer back in memory pool.
func putBuf(buf *byteutil.Buffer) {
	if cap(buf.B) > int(^uint16(0)) {
		return // drop large buffers
	}
	buf.B = buf.B[:0]
	bufpool.Put(buf)
}
