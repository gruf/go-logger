# go-logger

Simplistic, performant, opinionated levelled logging package.

Pass `-tags=kvformat` to your build to use the custom formatter in `go-kv` when printing fields (see package for more details).

Import with `"codeberg.org/gruf/go-logger/v4"`. Only v4 is supported going forward.