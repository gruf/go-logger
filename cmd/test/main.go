package main

import (
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v4"
	"codeberg.org/gruf/go-logger/v4/log"
)

func main() {
	log.Logger.SetFlags(logger.LCaller | logger.LTimestamp)

	logAtLevel(log.Print, log.Printf, log.PrintKV, log.PrintKVs)
	logAtLevel(log.Trace, log.Tracef, log.TraceKV, log.TraceKVs)
	logAtLevel(log.Debug, log.Debugf, log.DebugKV, log.DebugKVs)
	logAtLevel(log.Info, log.Infof, log.InfoKV, log.InfoKVs)
	logAtLevel(log.Warn, log.Warnf, log.WarnKV, log.WarnKVs)
	logAtLevel(log.Error, log.Errorf, log.ErrorKV, log.ErrorKVs)
	logAtLevel(log.Panic, log.Panicf, log.PanicKV, log.PanicKVs)

	e := log.Entry().With(kv.Field{K: "key", V: "value"})
	logAtLevel(e.Trace, e.Tracef, e.TraceKV, e.TraceKVs)
	logAtLevel(e.Debug, e.Debugf, e.DebugKV, e.DebugKVs)
	logAtLevel(e.Info, e.Infof, e.InfoKV, e.InfoKVs)
	logAtLevel(e.Warn, e.Warnf, e.WarnKV, e.WarnKVs)
	logAtLevel(e.Error, e.Errorf, e.ErrorKV, e.ErrorKVs)
	logAtLevel(e.Panic, e.Panicf, e.PanicKV, e.PanicKVs)

	log := log.Logger

	logAtLevel(log.Print, log.Printf, log.PrintKV, log.PrintKVs)
	logAtLevel(log.Trace, log.Tracef, log.TraceKV, log.TraceKVs)
	logAtLevel(log.Debug, log.Debugf, log.DebugKV, log.DebugKVs)
	logAtLevel(log.Info, log.Infof, log.InfoKV, log.InfoKVs)
	logAtLevel(log.Warn, log.Warnf, log.WarnKV, log.WarnKVs)
	logAtLevel(log.Error, log.Errorf, log.ErrorKV, log.ErrorKVs)
	logAtLevel(log.Panic, log.Panicf, log.PanicKV, log.PanicKVs)

	e = log.Entry().With(kv.Field{K: "key", V: "value"})
	logAtLevel(e.Trace, e.Tracef, e.TraceKV, e.TraceKVs)
	logAtLevel(e.Debug, e.Debugf, e.DebugKV, e.DebugKVs)
	logAtLevel(e.Info, e.Infof, e.InfoKV, e.InfoKVs)
	logAtLevel(e.Warn, e.Warnf, e.WarnKV, e.WarnKVs)
	logAtLevel(e.Error, e.Errorf, e.ErrorKV, e.ErrorKVs)
	logAtLevel(e.Panic, e.Panicf, e.PanicKV, e.PanicKVs)
}

func logAtLevel(
	print func(...any),
	printf func(string, ...any),
	printkv func(string, any),
	printkvs func(...kv.Field),
) {
	defer func() {
		if r := recover(); r != nil {
			defer func() {
				if r := recover(); r != nil {
					defer func() {
						if r := recover(); r != nil {
							defer func() { recover() }()
							printkvs(kv.Field{K: "hello", V: "world"})
						}
					}()
					printkv("hello", "world")
				}
			}()
			printf("printf: hello %s", "world")
		}
	}()
	print("print: hello world")
	printf("printf: hello %s", "world")
	printkv("hello", "world")
	printkvs(kv.Field{K: "hello", V: "world"})
}
