package logger

import (
	"fmt"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
)

type Entry struct {
	Lvl  LEVEL
	Data []kv.Field
	Msg  string
	Out  *Logger
}

func (e Entry) With(fields ...kv.Field) Entry {
	return Entry{
		Lvl:  e.Lvl,
		Data: append(e.Data, fields...),
		Msg:  e.Msg,
		Out:  e.Out,
	}
}

func (e Entry) Trace(args ...interface{}) {
	e.Lvl = TRACE
	e.Msg = fmt.Sprint(args...)
	e.Write(2)
}

func (e Entry) Tracef(msg string, args ...interface{}) {
	e.Lvl = TRACE
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(2)
}

func (e Entry) TraceKV(key string, value any) {
	e.Lvl = TRACE
	e.Data = append(e.Data, kv.Field{K: key, V: value})
	e.Write(2)
}

func (e Entry) TraceKVs(fields ...kv.Field) {
	e.Lvl = TRACE
	e.Data = append(e.Data, fields...)
	e.Write(2)
}

func (e Entry) Debug(args ...interface{}) {
	e.Lvl = DEBUG
	e.Msg = fmt.Sprint(args...)
	e.Write(2)
}

func (e Entry) Debugf(msg string, args ...interface{}) {
	e.Lvl = DEBUG
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(2)
}

func (e Entry) DebugKV(key string, value any) {
	e.Lvl = DEBUG
	e.Data = append(e.Data, kv.Field{K: key, V: value})
	e.Write(2)
}

func (e Entry) DebugKVs(fields ...kv.Field) {
	e.Lvl = DEBUG
	e.Data = append(e.Data, fields...)
	e.Write(2)
}

func (e Entry) Info(args ...interface{}) {
	e.Lvl = INFO
	e.Msg = fmt.Sprint(args...)
	e.Write(2)
}

func (e Entry) Infof(msg string, args ...interface{}) {
	e.Lvl = INFO
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(2)
}

func (e Entry) InfoKV(key string, value any) {
	e.Lvl = INFO
	e.Data = append(e.Data, kv.Field{K: key, V: value})
	e.Write(2)
}

func (e Entry) InfoKVs(fields ...kv.Field) {
	e.Lvl = INFO
	e.Data = append(e.Data, fields...)
	e.Write(2)
}

func (e Entry) Warn(args ...interface{}) {
	e.Lvl = WARN
	e.Msg = fmt.Sprint(args...)
	e.Write(2)
}

func (e Entry) Warnf(msg string, args ...interface{}) {
	e.Lvl = WARN
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(2)
}

func (e Entry) WarnKV(key string, value any) {
	e.Lvl = WARN
	e.Data = append(e.Data, kv.Field{K: key, V: value})
	e.Write(2)
}

func (e Entry) WarnKVs(fields ...kv.Field) {
	e.Lvl = WARN
	e.Data = append(e.Data, fields...)
	e.Write(2)
}

func (e Entry) Error(args ...interface{}) {
	e.Lvl = ERROR
	e.Msg = fmt.Sprint(args...)
	e.Write(2)
}

func (e Entry) Errorf(msg string, args ...interface{}) {
	e.Lvl = ERROR
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(2)
}

func (e Entry) ErrorKV(key string, value any) {
	e.Lvl = ERROR
	e.Data = append(e.Data, kv.Field{K: key, V: value})
	e.Write(2)
}

func (e Entry) ErrorKVs(fields ...kv.Field) {
	e.Lvl = ERROR
	e.Data = append(e.Data, fields...)
	e.Write(2)
}

func (e Entry) Panic(args ...interface{}) {
	e.Lvl = PANIC
	e.Msg = fmt.Sprint(args...)
	e.Write(2)
	panic(e.Msg)
}

func (e Entry) Panicf(msg string, args ...interface{}) {
	e.Lvl = PANIC
	e.Msg = fmt.Sprintf(msg, args...)
	e.Write(2)
	panic(e.Msg)
}

func (e Entry) PanicKV(key string, value any) {
	e.Lvl = PANIC
	e.Data = append(e.Data, kv.Field{K: key, V: value})
	e.Write(2)
	panic(kv.Field{K: key, V: value}.String())
}

func (e Entry) PanicKVs(fields ...kv.Field) {
	e.Lvl = PANIC
	e.Data = append(e.Data, fields...)
	e.Write(2)
	panic(kv.Fields(fields).String())
}

// Write will write current entry to log with calldepth.
//
// Note this function cannot be inlined, to ensure expected
// and consistent behaviour in setting trace / caller info.
//
//go:noline
func (e Entry) Write(calldepth int) {
	e.Out.Write(calldepth+1, e.Lvl, func(buf *byteutil.Buffer) {
		if len(e.Data) > 0 {
			// Append formatted kv fields
			kv.Fields(e.Data).AppendFormat(buf, false)
			buf.B = append(buf.B, ' ')
		}

		if e.Msg != "" {
			// Append quoted message string
			buf.B = append(buf.B, `msg=`...)
			kv.AppendQuoteString(buf, e.Msg)
		}
	})
}
