module codeberg.org/gruf/go-logger/v4

go 1.19

require (
	codeberg.org/gruf/go-byteutil v1.1.2
	codeberg.org/gruf/go-kv v1.6.4
	codeberg.org/gruf/go-log v1.0.5
	codeberg.org/gruf/go-middleware v1.1.0
	github.com/sirupsen/logrus v1.8.1
)

require (
	codeberg.org/gruf/go-buf v1.0.0 // indirect
	codeberg.org/gruf/go-ctx v1.0.2 // indirect
	codeberg.org/gruf/go-ulid v1.0.0 // indirect
	github.com/felixge/httpsnoop v1.0.2 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
