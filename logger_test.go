package logger_test

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"testing"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v4"
	"codeberg.org/gruf/go-logger/v4/log"
)

var (
	testArgs = [][]interface{}{
		{0, 1, 2, 3, 4},
		{"hello", "world!"},
		{"random", 0, 1, 2.2, 3.3, "arguments"},
		{[]int{0, 1, 2, 3}, []string{"hello", "world"}, map[string]string{"hello": "world"}},
		{http.Client{}, sync.Mutex{}},
		{nil, nil, nil, nil},
	}

	testFmts = []struct {
		fmt string
		arg []interface{}
	}{
		{
			fmt: "hello world: %q",
			arg: []interface{}{"hello world"},
		},
		{
			fmt: "no operators here",
			arg: []interface{}{"oh no unexpected!"},
		},
		{
			fmt: "0, 1, %d, %f, %s",
			arg: []interface{}{2, 3.0, "4"},
		},
		{
			fmt: "%d %d %d %d",
			arg: []interface{}{"oh no bad and missing operators"},
		},
		{
			fmt: "%#v",
			arg: []interface{}{http.Client{}},
		},
		{
			fmt: "%+v",
			arg: []interface{}{map[string]interface{}{"key": "value", "hello": "world"}},
		},
	}

	testFields = []kv.Fields{
		{
			kv.Field{K: "hello", V: "world!"},
			kv.Field{K: "client", V: http.Client{}},
		},
		{
			kv.Field{K: "map", V: map[string]string{"map": "of values"}},
			kv.Field{K: "slice", V: []string{"slice", "of", "values"}},
		},
		{
			kv.Field{K: "nil", V: nil},
			kv.Field{K: "hmmm", V: "watcha sayyyy"},
		},
		{
			kv.Field{K: "int", V: 420},
			kv.Field{K: "float", V: 6.9},
		},
		{
			kv.Field{K: "complex", V: complex(120, 120)},
			kv.Field{K: "shrug", V: "\n\n\n\n\n\n\n\n\n\n\n\n"},
		},
	}
)

func TestLimitByLevel(t *testing.T) {
	// Don't include timestamp
	// or caller information.
	log.Logger.SetFlags(0)

	// Output to buffer
	buf := bytes.Buffer{}
	log.Logger.SetOutput(&buf)

	testLog := func(do func(), expect bool) {
		do()
		if !expect && buf.Len() > 0 {
			t.Error("log should not have produced output")
		} else if expect && buf.Len() == 0 {
			t.Error("log should have produced output")
		}
		buf.Reset()
	}

	for _, lvl := range []logger.LEVEL{
		logger.ALL,
		logger.TRACE,
		logger.DEBUG,
		logger.INFO,
		logger.WARN,
		logger.ERROR,
		logger.PANIC,
		logger.UNSET,
	} {
		// Update log level
		log.Logger.SetLevel(lvl)

		canTrace := lvl.CanLog(logger.TRACE)
		canDebug := lvl.CanLog(logger.DEBUG)
		canInfo := lvl.CanLog(logger.INFO)
		canWarn := lvl.CanLog(logger.WARN)
		canError := lvl.CanLog(logger.ERROR)
		canPanic := lvl.CanLog(logger.PANIC)
		t.Logf("case=%+v", struct {
			canTrace bool
			canDebug bool
			canInfo  bool
			canWarn  bool
			canError bool
			canPanic bool
		}{
			canTrace: canTrace,
			canDebug: canDebug,
			canInfo:  canInfo,
			canWarn:  canWarn,
			canError: canError,
			canPanic: canPanic,
		})

		// TRACE level
		testLog(func() {
			log.Trace("test log")
		}, canTrace)
		testLog(func() {
			log.Tracef("test log")
		}, canTrace)
		testLog(func() {
			log.TraceKV("test", "log")
		}, canTrace)
		testLog(func() {
			log.TraceKVs(kv.Field{K: "test", V: "log"})
		}, canTrace)

		// DEBUG level
		testLog(func() {
			log.Debug("test log")
		}, canDebug)
		testLog(func() {
			log.Debugf("test log")
		}, canDebug)
		testLog(func() {
			log.DebugKV("test", "log")
		}, canDebug)
		testLog(func() {
			log.DebugKVs(kv.Field{K: "test", V: "log"})
		}, canDebug)

		// INFO level
		testLog(func() {
			log.Info("test log")
		}, canInfo)
		testLog(func() {
			log.Infof("test log")
		}, canInfo)
		testLog(func() {
			log.InfoKV("test", "log")
		}, canInfo)
		testLog(func() {
			log.InfoKVs(kv.Field{K: "test", V: "log"})
		}, canInfo)

		// WARN level
		testLog(func() {
			log.Warn("test log")
		}, canWarn)
		testLog(func() {
			log.Warnf("test log")
		}, canWarn)
		testLog(func() {
			log.WarnKV("test", "log")
		}, canWarn)
		testLog(func() {
			log.WarnKVs(kv.Field{K: "test", V: "log"})
		}, canWarn)

		// ERROR level
		testLog(func() {
			log.Error("test log")
		}, canError)
		testLog(func() {
			log.Errorf("test log")
		}, canError)
		testLog(func() {
			log.ErrorKV("test", "log")
		}, canError)
		testLog(func() {
			log.ErrorKVs(kv.Field{K: "test", V: "log"})
		}, canError)

		// PANIC level
		testLog(func() {
			defer func() { _ = recover() }()
			log.Panic("test log")
		}, canPanic)
		testLog(func() {
			defer func() { _ = recover() }()
			log.Panicf("test log")
		}, canPanic)
		testLog(func() {
			defer func() { _ = recover() }()
			log.PanicKV("test", "log")
		}, canPanic)
		testLog(func() {
			defer func() { _ = recover() }()
			log.PanicKVs(kv.Field{K: "test", V: "log"})
		}, canPanic)
	}
}

func TestLevelOutput(t *testing.T) {
	// Don't include timestamp
	// or caller information.
	log.Logger.SetFlags(0)

	// Output to buffer
	var buf byteutil.Buffer
	log.Logger.SetOutput(&buf)

	// Reset logging level
	log.Logger.SetLevel(logger.ALL)

	testLog := func(do func(), match func(string) bool) {
		do()
		if str := buf.String(); !match(strings.TrimSpace(str)) {
			t.Errorf("unexpected log output: %s", str)
		}
		buf.Reset()
	}

	for _, arg := range testArgs {
		var buf byteutil.Buffer
		msg := fmt.Sprint(arg...)
		kv.AppendQuoteString(&buf, msg)
		expect := "msg=" + buf.String()
		t.Logf("expect: %s", expect)

		testLog(func() {
			log.Trace(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=TRACE ") &&
				(strings.TrimPrefix(out, "level=TRACE ") == expect)
		})

		testLog(func() {
			log.Debug(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=DEBUG ") &&
				(strings.TrimPrefix(out, "level=DEBUG ") == expect)
		})

		testLog(func() {
			log.Info(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=INFO ") &&
				(strings.TrimPrefix(out, "level=INFO ") == expect)
		})

		testLog(func() {
			log.Warn(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=WARN ") &&
				(strings.TrimPrefix(out, "level=WARN ") == expect)
		})

		testLog(func() {
			log.Error(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=ERROR ") &&
				(strings.TrimPrefix(out, "level=ERROR ") == expect)
		})

		testLog(func() {
			defer func() {
				if r := recover(); r != msg {
					t.Errorf("unexpected panic result: %v", r)
				}
			}()
			log.Panic(arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=PANIC ") &&
				(strings.TrimPrefix(out, "level=PANIC ") == expect)
		})
	}

	for _, format := range testFmts {
		var buf byteutil.Buffer
		msg := fmt.Sprintf(format.fmt, format.arg...)
		kv.AppendQuoteString(&buf, msg)
		expect := "msg=" + buf.String()
		t.Logf("expect: %s", expect)

		testLog(func() {
			log.Tracef(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=TRACE ") &&
				(strings.TrimPrefix(out, "level=TRACE ") == expect)
		})

		testLog(func() {
			log.Debugf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=DEBUG ") &&
				(strings.TrimPrefix(out, "level=DEBUG ") == expect)
		})

		testLog(func() {
			log.Infof(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=INFO ") &&
				(strings.TrimPrefix(out, "level=INFO ") == expect)
		})

		testLog(func() {
			log.Warnf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=WARN ") &&
				(strings.TrimPrefix(out, "level=WARN ") == expect)
		})

		testLog(func() {
			log.Errorf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=ERROR ") &&
				(strings.TrimPrefix(out, "level=ERROR ") == expect)
		})

		testLog(func() {
			defer func() {
				if r := recover(); r != msg {
					t.Errorf("unexpected panic result: %v", r)
				}
			}()
			log.Panicf(format.fmt, format.arg...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=PANIC ") &&
				(strings.TrimPrefix(out, "level=PANIC ") == expect)
		})
	}

	for _, fields := range testFields {
		for _, field := range fields {
			expect := field.String()
			t.Logf("expect: %s", expect)

			testLog(func() {
				log.TraceKV(field.K, field.V)
			}, func(out string) bool {
				return strings.HasPrefix(out, "level=TRACE ") &&
					(strings.TrimPrefix(out, "level=TRACE ") == expect)
			})

			testLog(func() {
				log.DebugKV(field.K, field.V)
			}, func(out string) bool {
				return strings.HasPrefix(out, "level=DEBUG ") &&
					(strings.TrimPrefix(out, "level=DEBUG ") == expect)
			})

			testLog(func() {
				log.InfoKV(field.K, field.V)
			}, func(out string) bool {
				return strings.HasPrefix(out, "level=INFO ") &&
					(strings.TrimPrefix(out, "level=INFO ") == expect)
			})

			testLog(func() {
				log.WarnKV(field.K, field.V)
			}, func(out string) bool {
				return strings.HasPrefix(out, "level=WARN ") &&
					(strings.TrimPrefix(out, "level=WARN ") == expect)
			})

			testLog(func() {
				log.ErrorKV(field.K, field.V)
			}, func(out string) bool {
				return strings.HasPrefix(out, "level=ERROR ") &&
					(strings.TrimPrefix(out, "level=ERROR ") == expect)
			})

			testLog(func() {
				defer func() {
					if r := recover(); r != expect {
						t.Errorf("unexpected panic result: %v", r)
					}
				}()
				log.PanicKV(field.K, field.V)
			}, func(out string) bool {
				return strings.HasPrefix(out, "level=PANIC ") &&
					(strings.TrimPrefix(out, "level=PANIC ") == expect)
			})
		}

		expect := fields.String()
		t.Logf("expect: %s", expect)

		testLog(func() {
			log.TraceKVs(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=TRACE ") &&
				(strings.TrimPrefix(out, "level=TRACE ") == expect)
		})

		testLog(func() {
			log.DebugKVs(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=DEBUG ") &&
				(strings.TrimPrefix(out, "level=DEBUG ") == expect)
		})

		testLog(func() {
			log.InfoKVs(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=INFO ") &&
				(strings.TrimPrefix(out, "level=INFO ") == expect)
		})

		testLog(func() {
			log.WarnKVs(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=WARN ") &&
				(strings.TrimPrefix(out, "level=WARN ") == expect)
		})

		testLog(func() {
			log.ErrorKVs(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=ERROR ") &&
				(strings.TrimPrefix(out, "level=ERROR ") == expect)
		})

		testLog(func() {
			defer func() {
				if r := recover(); r != expect {
					t.Errorf("unexpected panic result: %v", r)
				}
			}()
			log.PanicKVs(fields...)
		}, func(out string) bool {
			return strings.HasPrefix(out, "level=PANIC ") &&
				(strings.TrimPrefix(out, "level=PANIC ") == expect)
		})
	}
}
